// This file is part of dotsrc_viewer.
// 
// dotsrc_viewer is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// dotsrc_viewer is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with dotsrc_viewer.  If not, see <https://www.gnu.org/licenses/>.

export function bytes_to_human(bytes: number): string {
    const k = 1024;
    const dm = 2;
    const sizes = ['B', 'KB', 'MB', 'GB', 'TB'];

    let i = Math.floor(Math.log(bytes) / Math.log(k));
    if (i >= sizes.length) {
        i = sizes.length-1;
    }
    return `${(bytes / Math.pow(k, i)).toFixed(dm)} ${sizes[i]}`;
}
